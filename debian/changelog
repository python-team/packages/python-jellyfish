python-jellyfish (1.1.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
    - Supports pyo3 0.22.2 (Closes: #1078375)
  * Clean debian/cargo_home/.global-cache.
  * Bump Standards-Version to 4.7.0, no changes needed.

 -- Stefano Rivera <stefanor@debian.org>  Sat, 17 Aug 2024 22:55:52 +0200

python-jellyfish (1.0.4-1) unstable; urgency=medium

  * New upstream version

 -- Julian Gilbey <jdg@debian.org>  Tue, 16 Jul 2024 23:08:53 +0100

python-jellyfish (0.10.0-3) unstable; urgency=medium

  * Fix binary-only build (closes: #1057730)
  * The 0.10.0 version also fixes the deprecated 'u' format
    (closes: #1014932)

 -- Julian Gilbey <jdg@debian.org>  Sun, 10 Dec 2023 14:14:24 +0000

python-jellyfish (0.10.0-2) unstable; urgency=medium

  * Fix clean target

 -- Julian Gilbey <jdg@debian.org>  Thu, 07 Dec 2023 08:53:11 +0000

python-jellyfish (0.10.0-1) unstable; urgency=medium

  [ Diego M. Rodriguez ]
  * d/test: fix autopkgtest not running tests.
    Thanks to Julian Gilbey for the report and the solution (Closes: #1001696)

  [ Julian Gilbey ]
  * New upstream version 0.10.0 (closes: #1055555)
  * Move documentation build system from sphinx to mkdocs
  * Remove no-longer-extant manpage
  * Fix uninitialised variable warning
  * Change uploaders to myself (Diego is retiring from Debian)

 -- Julian Gilbey <jdg@debian.org>  Thu, 07 Dec 2023 07:13:58 +0000

python-jellyfish (0.8.9-1) unstable; urgency=medium

  * New upstream version 0.8.9 (Closes: #997918)
  * d/patches: refresh patches

 -- Diego M. Rodriguez <diego@moreda.io>  Fri, 29 Oct 2021 16:52:29 +0200

python-jellyfish (0.8.8-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Apply multi-arch hints.
    + python-jellyfish-doc: Add Multi-Arch: foreign.

  [ Diego M. Rodriguez ]
  * d/watch: bump version to 4
  * New upstream version 0.8.8
  * d/control: bump Standards-Version to 4.6.0 (no changes needed)
  * d/copyright,doc: update upstream email address
  * d/copyright: refresh copyright entries
  * d/rules: avoid always building doc package
  * d/rules: enable all hardening flags
  * d/clean: clean egg-info entries

 -- Diego M. Rodriguez <diego@moreda.io>  Mon, 20 Sep 2021 13:56:04 +0000

python-jellyfish (0.8.2-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository.
  * Update standards version to 4.5.0, no changes needed.

  [ Diego M. Rodriguez ]
  * New upstream version 0.8.2
  * Add salsa CI using default pipelines
  * d/control: bump debhelper-compat to 13
  * d/control: add Rules-Requires-Root

 -- Diego M. Rodriguez <diego@moreda.io>  Thu, 18 Jun 2020 16:18:42 +0200

python-jellyfish (0.7.2-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Diego M. Rodriguez ]
  * Fix autopkgtest support for multiple Python versions.
    Thanks to Steve Langasek for the patch (Closes: #943454)
  * New upstream release

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository, Repository-Browse.

 -- Diego M. Rodriguez <diego@moreda.io>  Thu, 02 Jan 2020 12:11:53 +0100

python-jellyfish (0.6.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * Use debhelper-compat instead of debian/compat.
  * Use 'python3 -m sphinx' instead of sphinx-build for building docs.
  * Bump Standards-Version to 4.4.0.
  * Drop Python2 support
  * d/tests/py3: Cleanup and test installed package
  * Bump debhelper compat level to 12

  [ Jelmer Vernooĳ ]
  * Remove unnecessary X-Python{,3}-Version field in debian/control.

  [ Diego M. Rodriguez ]
  * Refresh patches after git-dpm to gbp pq conversion
  * d/control: update python3-sphinx build-depends
  * d/watch: update URL to https
  * New upstream release
  * Bump Standards-Version to 4.3.0
  * d/control: update maintainer email address

 -- Diego M. Rodriguez <diego@moreda.io>  Tue, 23 Jul 2019 20:25:54 +0200

python-jellyfish (0.5.6-3) unstable; urgency=medium

  * Move man page to section 3. (Closes: #838681)

 -- Diego M. Rodriguez <diego.plan9@gmail.com>  Tue, 27 Sep 2016 16:14:24 +0200

python-jellyfish (0.5.6-2) unstable; urgency=medium

  * Move python-sphinx to Build-Depends. (Closes: #838677)
  * Add Vcs-Git and Vcs-Browser fields.

 -- Diego M. Rodriguez <diego.plan9@gmail.com>  Sun, 25 Sep 2016 17:55:05 +0200

python-jellyfish (0.5.6-1) unstable; urgency=medium

  * Initial release. (Closes: #806716)

 -- Diego M. Rodriguez <diego.plan9@gmail.com>  Thu, 15 Sep 2016 17:16:35 +0200
